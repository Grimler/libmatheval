# Checking the constants.                    -*- Autotest -*-

# Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011, 2012,
# 2013 Free Software Foundation, Inc.
#
# This file is part of GNU libmatheval
#
# GNU libmatheval is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# GNU libmatheval is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU libmatheval.  If not, see
# <http://www.gnu.org/licenses/>.

AT_BANNER([[Checking evaluating constants.]])

AT_SETUP([Check constants.])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "e"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [2.718281828459045], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "log2e"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [1.4426950408889634], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "log10e"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [0.4342944819032518], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "ln2"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [0.6931471805599453], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "ln10"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [2.302585092994046], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "pi"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [3.141592653589793], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "pi_2"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [1.5707963267948966], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "pi_4"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [0.7853981633974483], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "1_pi"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [0.3183098861837907], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "2_pi"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [0.6366197723675814], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "2_sqrtpi"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [1.1283791670955126], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "sqrt2"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [1.4142135623730951], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "sqrt1_2"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [0.7071067811865476], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "e^ln10"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [10.0], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "pi*1_pi"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [1.0], [ignore])

AT_DATA([constant.scm],
[[
(define f (evaluator-create "sqrt2/sqrt1_2"))
(display (evaluator-evaluate-x f 0))
]])

AT_CHECK([matheval.sh constant.scm], [ignore], [2.0], [ignore])

AT_CLEANUP
